<?php

spl_autoload_extensions('.php');
spl_autoload_register('AutoLoader::LibraryLoader');
spl_autoload_register('AutoLoader::ModelLoader');
spl_autoload_register('AutoLoader::ViewHelperLoader');
spl_autoload_register('AutoLoader::ControllerLoader');

class Autoloader {

	public static function LibraryLoader($className) {
		$filename = strtolower($className) . '.php';
		$file = ROOT . DS . 'library' . DS . $filename;
		if (!file_exists($file)) {
			return false;
		}
		require_once($file);
	}

	public static function ModelLoader($className) {
		$filename = strtolower($className) . '.php';
		$file = ROOT . DS . 'application' . DS . 'models'
			. DS . $filename;
		if (!file_exists($file)) {
			return false;
		}
		require_once($file);
	}

	public static function ControllerLoader($className) {
		$filename = strtolower($className) . '.php';
		$file = ROOT . DS . 'application' . DS . 'controllers'
			. DS . $filename;
		if (!file_exists($file)) {
			return false;
		}
		require_once($file);
	}

	public static function ViewHelperLoader($className) {
		$filename = strtolower($className) . '.php';
		$file = ROOT . DS . 'application' . DS . 'helpers'
			. DS . $filename;
		if (!file_exists($file)) {
			return false;
		}
		require_once($file);
	}
}