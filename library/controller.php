<?php

class Controller {
	const SESSION_USER_ID = 'userId';
	const SESSION_ORIG_URL = 'origURL';


	protected $_controller;
	protected $_action;
	protected $_template;

	public $doNotRenderHeader;
	public $render;

	public function beforeAction() {

	}

	function __construct($controller, $action) {

		if (session_status() == PHP_SESSION_NONE) {
    		session_start();
		}

		global $inflect;

		$this->_controller = ucfirst($controller);
		$this->_action = $action;

		$this->doNotRenderHeader = 0;
		$this->render = 1;
		$this->_template = new Template($controller,$action);
	}

	public function afterAction() {

	}

	function set($name,$value) {
		$this->_template->set($name,$value);
	}

	function addToJs($js) {
		$this->_template->addToJs($js);
	}

	function addToCss($css) {
		$this->_template->addToCss($css);
	}

	function addToMeta($name, $content) {
		$this->_template->addToMeta($name, $content);
	}

	function setTitle($title) {
		$this->_template->setTitle($title);
	}

	public function redirect($url, $statusCode = 303) {
   		header('Location: ' . $url, true, $statusCode);
   		die();
	}

	public function forward($controller, $action, $params=array()) {
		$this->render = 0;
		performAction($controller, $action, $params);
	}

	function __destruct() {
		if ($this->render) {
			$this->_template->render($this->doNotRenderHeader);
		}
	}

}