<?php
/**
 * Abstract implementation of the view helper object.
 */
abstract class ViewHelper {
	/**
	 * The view object this helper was called from
	 * @var View
	 */
	protected $_view;

	/**
	 * A constructor which sets the view in the helper
	 * @param View $view The parent view which called this helper.
	 */
	public function __construct($view) {
		$this->_view = $view;
	}
}