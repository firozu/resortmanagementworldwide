<?php

class HTML {
	private $js = array();

	function shortenUrls($data) {
		$data = preg_replace_callback('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.]*(\?\S+)?)?)?)@', array(get_class($this), '_fetchTinyUrl'), $data);
		return $data;
	}

	private function _fetchTinyUrl($url) {
		$ch = curl_init();
		$timeout = 5;
		curl_setopt($ch,CURLOPT_URL,'http://tinyurl.com/api-create.php?url='.$url[0]);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,$timeout);
		$data = curl_exec($ch);
		curl_close($ch);
		return '<a href="'.$data.'" target = "_blank" >'.$data.'</a>';
	}

	function sanitize($data) {
		return mysql_real_escape_string($data);
	}

	function link($text,$path,$prompt = null,$confirmMessage = "Are you sure?") {
		$path = str_replace(' ','-',$path);
		$path = BASE_PATH . "/" . $path;
		if ($prompt) {
			$data = '<a href="'. $path . '" onclick="javascript:return confirm(\''.$confirmMessage.'\')">'.$text.'</a>';
		} else {
			$data = '<a href="'.$path.'">'.$text.'</a>';
		}
		return $data;
	}

	function displayCss($cssArray) {
		$data = '';
		foreach ($cssArray as $css) {
			$data .= $this->includeCss($css);
		}
		return $data;
	}

	function displayJs($jsArray) {
		$data = '';
		foreach ($jsArray as $js) {
			$data .= $this->includeJs($js);
		}
		return $data;
	}

	function displayMeta($metaArray) {
		$data = '';
		foreach ($metaArray as $meta) {
			$data .= $this->includeMeta($meta);
		}
		return $data;
	}

	function includeJs($fileName) {
		$data = '<script src="'.BASE_PATH.'/js/'.$fileName.'.js"></script>';
		return $data;
	}

	function includeCss($fileName) {
		$data = '<link rel="stylesheet" type="text/css" href="'.BASE_PATH.'/css/'.$fileName.'.css">';
		return $data;
	}

	function includeMeta($metaData) {
		$data = "<meta name={$metaData['name']} content={$metaData['content']} >";
		return $data;
	}
}