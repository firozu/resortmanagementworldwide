<?php

class Template {

	protected $variables = array();
	protected $_js = array();
	protected $_css = array();
	protected $_meta = array();
	protected $_helpers = array();
	protected $_title;
	protected $_controller;
	protected $_action;

	function __construct($controller,$action) {
		$this->_controller = $controller;
		$this->_action = $action;
	}

	/** Set Variables **/

	function set($name,$value) {
		$this->variables[$name] = $value;
	}

	function addToJs($js) {
		array_push($this->_js, $js);
	}

	function addToCss($css) {
		array_push($this->_css, $css);
	}

	function addToMeta($name, $content) {
		$this->_meta[] = array(
			'name' => $name,
			'content' => $content
		);
	}

	function setTitle($title) {
		$this->_title = $title;
	}

	/**
	 * Allows people to call view helpers by doing a $viewObject->helperName().
	 * @param string $name The name of the helper to call
	 * @param array $args The arguments to pass to the helper
	 */
	public function __call($name, $args) {
		$class = strtolower($name);
		if (!array_key_exists($class, $this->_helpers)) {
			if (!class_exists($class)) {
				throw new Exception("Unable to locate view helper class (". $class. ")");
			}

			$obj = new $class($this);

			if (!($obj instanceof ViewHelper)) {
				throw new Exception("View helper class is not of type ViewHelper");
			}

			$this->_helpers[$class] = $obj;
		}

		return call_user_func_array(array($this->_helpers[$class],'render'), $args);
	}


	/** Display Template **/

    function render($doNotRenderHeader = 0) {

		$html = new HTML();
		$header = "";
		$body = "";
		$footer = "";

		$body = $this->getView($this->_action . '.php', $html);

		if ($doNotRenderHeader == 0) {
			$header = $this->getView('header.php', $html);
			$footer = $this->getView('footer.php', $html);
		}

		echo $header;
		echo $body;
		echo $footer;
    }

    private function getView($fileName, $html) {
    	ob_start();
		extract($this->variables);

    	$file = ROOT . DS . 'application' . DS . 'views' . DS
    		. $this->_controller . DS . $fileName;
		if (file_exists($file)) {
			include ($file);
		} else {
			include (ROOT . DS . 'application' . DS . 'views' . DS
				. $fileName);
		}

		return ob_get_clean();
    }

}