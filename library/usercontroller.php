<?php

class UserController extends Controller {

	protected $_user = NULL;

	public function beforeAction() {
		global $url;
		$_SESSION[self::SESSION_ORIG_URL] = BASE_PATH . '/' . $url;

		if (!$this->isUserLoggedIn()) {
			$this->redirect(BASE_PATH . '/login');
		}
	}

	public function afterAction() {

	}

	public function isUserLoggedIn() {
		if (isset($_SESSION[self::SESSION_USER_ID])) {
			$userModel = new User();
			$userModel->id = $_SESSION[self::SESSION_USER_ID];
			$this->_user = $userModel->search();
			$this->set('user', $this->_user);
		}
		return isset($this->_user);
	}

	public function isAdmin() {
		return isset($this->_user) && $this->_user['User']['is_admin'];
	}
}