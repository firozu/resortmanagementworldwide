$(function() {
	$('#searchBar').submit(function() {
		var uniqueId = $('#propertyId').val();
		if (uniqueId) {
			window.location.href = this.action + uniqueId;
		}
	});
});