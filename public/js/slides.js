$(function() {
    var slides = $('.slides');

	//initialize slideshow plugin
    if (slides.length > 0) {
        slides.slidesjs({
            width: 940,
            height: 528,
            navigation: {
                active: false
            },
            play: {
                active: false,
                interval: 3000,
                auto: true,
                effect: "fade"
            },
            effect: {
                fade: {
                    speed: 2000
                }
            },
            pagination: {
                active: false
            }
        });
    }
});