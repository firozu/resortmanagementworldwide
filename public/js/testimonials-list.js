$(window).bind('load', function() {
	var propertiesGrid = $('.content .propertiesGrid');
	var testimonialsList = $('.content .testimonialsList');
	var height = propertiesGrid.height();

	if (propertiesGrid.length == 1 && height < testimonialsList.height()) {
		testimonialsList.height(height);
	}
});