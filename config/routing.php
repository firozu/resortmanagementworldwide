<?php

$routing = array(
	'/admin\/(.*?)\/(.*?)\/(.*)/' => 'admin/\1_\2/\3',
	'/admin\/(.*?)\/(.*?)/' => 'admin/\1_\2',
);

$default['controller'] = 'index';
$default['action'] = 'index';