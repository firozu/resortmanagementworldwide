<div class="name smoothBackground">
	<?=$property['Property']['name'];?>
</div>
<div class="row">
	<div class="grid_8 col">
		<div class="slidesContainer">
			<?=$this->slides('propertiesSlides', $dir, $images);?>
		</div>
	</div>
	<div class="grid_4 col">
		<div class="contentHeader">Property Details</div>
		<div class="content">
			<table class="propertiesDetailTable m">
				<tr>
					<td>Price:</td>
					<td>$<?=number_format($property['Property']['price']);?></td>
				</tr>
				<tr>
					<td>SQFT:</td>
					<td><?=number_format($property['Property']['sqft']);?></td>
				</tr>
				<tr>
					<td>Bed:</td>
					<td><?=$property['Property']['bed'];?></td>
				</tr>
				<tr>
					<td>Bath:</td>
					<td><?=$property['Property']['bath'];?></td>
				</tr>
				<tr>
					<td>City:</td>
					<td><?=$property['Property']['city'];?></td>
				</tr>
				<tr>
					<td>State:</td>
					<td><?=$property['Property']['state'];?></td>
				</tr>
				<tr>
					<td>Unique Id:</td>
					<td><?=$property['Property']['unique_id'];?></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class="row">
	<div class="grid_12 col">
		<div class="contentHeader">Property Description</div>
		<div class="content">
			<div class="m">
				<?=$property['Property']['description'];?>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="grid_8 col">
		<div class="contentHeader">Property Inquiry</div>
		<div class="content">
			<div class="row m">
				<?=$this->inputText('firstName', array('label' => 'First Name'));?>
				<?=$this->inputText('lastName', array('label' => 'Last Name'));?>
			</div>
			<div class="row m">
				<?=$this->inputText('emailAddress', array('label' => 'Email Address'));?>
				<?=$this->inputText('cellPhone', array('label' => 'Phone Number'));?>
			</div>
			<div class="row m">
				<?=$this->textarea('message', array('label' => 'Message', 'class' => 'grid_12 col'));?>
			</div>
			<div class="row m">
				<?=$this->button('submit', array('content'=>'SUBMIT'));?>
			</div>
			<div class="clear"></div>

		</div>
	</div>
</div>
