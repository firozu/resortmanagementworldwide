<div class="container">
	<div class="contentHeader"> Log In </div>
	<div class="content">
		<form class="m" method="POST" action="<?=BASE_PATH . DS . 'login';?>">
			<div class="row">
				<?=$this->inputText('email', $emailParams);?>
				<?=$this->inputText('password', $passwordParams);?>
			</div>
			<div class="row">
				<?=$this->button('login', $loginParams);?>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>