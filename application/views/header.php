<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<title><?=$this->_title;?></title>
	<?php
		echo $html->displayMeta($this->_meta);
		echo $html->includeJs('jquery/jquery-2.1.0.min');
		echo $html->includeJs('main');
		echo $html->displayJs($this->_js);
		echo $html->displayCss($this->_css);
		echo $html->includeCss('main');
	?>
</head>
<body>
	<div id="header" class="smoothBackground">
		<div class="row container bold">
			<ul id="menu" class="grid_8 col">
				<li><a href="<?=BASE_PATH;?>">Home</a></li>
			  	<li><a href="<?=BASE_PATH . '/properties/view';?>">Properties</a></li>
			  	<li><a href="<?=BASE_PATH . '/contact';?>">Contact Us</a></li>
			  	<li><a href="<?=BASE_PATH . '/faq';?>">FAQs</a></li>
			</ul>
			<form id="searchBar" class="grid_4 " method="GET" action="<?=BASE_PATH . '/properties/search/';?>" onsubmit="return false;">
				<?=$this->inputText("propertyId", array('type' => 'search', 'placeholder' => 'Property Id', 'class' => 'searchBox'));?>
				<?=$this->button("search", array('class' => 'searchBtn', 'content' => '<img src="' . BASE_PATH . '/img/search-icon-orange.png" />'));?>
			</form>
			<div class="clear"></div>
		</div>
	</div>
	<div class='container main'>
		<div id='banner' class="grid_12 orangeBlackGradient">
			<div class="bannerContainer">
				<div class="grid_9 col">
					<div id='line1'>Resort Management</div>
					<div id='line2'>Worldwide</div>
				</div>
				<div class="grid_3 col">
					<div class="contactContainer">
						<div class="contactHeader bold">
							Contact Us:
						</div>
						<div class="contact">
							<a href="tel:+1-888-405-9721">1-888-405-9721</a>
						</div>
						<div class="contact">
							<a href="mailto:customerservice@resortmanagementworldwide.com" target="_top">Email Us</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clear"></div>