<div class="slidesContainer row">
	<?=$this->slides('mainSlides', $dir, $images);?>
</div>
<div class='row'>
	<div class="grid_8 col">
		<div class="contentHeader">Recently Added Properties</div>
		<div class="content">
			<?=$this->propertiesGrid($properties);?>
			<div class="clear"></div>
		</div>
	</div>
	<div class="grid_4 col">
		<div class="contentHeader">Testimonials</div>
		<div class="content">
			<?=$this->testimonialsList($testimonials);?>
		</div>
	</div>
</div>
<div class="row">
	<div class="grid_12 col">
		<div class="contentHeader">How To Book A TimeShare Rental</div>
		<div class="content">
			<div class="m">
				<p>
					Our rental process is very simple and all the heavy lifting is handled by our experienced vacation specialists.
					<ul>
						<li>Make an offer on one of our rental listings.</li>
						<li>Or call us at <a href="tel:+1-888-405-9721">1-888-405-9721</a> to speak to a licensed agent.</li>
						<li>We'll help you choose the best timeshare rental for your needs and budget.</li>
						<li>Sign and return our rental agreement.</li>
						<li>We will send you an electronic receipt for your rental.</li>
						<li>Next, we will send your reference number via email, followed within 10 business days by a confirmation letter and record of your reservation at the resort.</li>
						<li>Your reservation is 100% guaranteed by Resortmanagementworldwide.com.</li>
						<li>When your check-in date arrives, enjoy your timeshare rental!</li>
					</ul>
				</p>
				<p>
					A few disclaimers regarding our time share rental policies:
					<ul>
						<li>Both the renter and the timeshare owner must sign and return the rental agreement before your reservation is guaranteed.</li>
						<li>If the name on the credit card you are using is different than the name on the rental reservation, both parties must sign and return the rental agreement.</li>
						<li>For some resorts, rental confirmations are not released prior to 30 days from the check-in date.</li>
					</ul>
				</p>
				<p class="text-large bold">
					Booking last minute?
				</p>
				<p>
					Booking at the last minute? Our licensed agents have you covered. In fact, when booking within a two week window, we can help you save even more. Many of our rental listings are part of an automatic price drop program in which rental prices drop repeatedly as the check-in date looms closer and closer. If you find yourself in need of a last-minute getaway, be sure to call our vacation specialists at <a href="tel:+1-888-405-9721">1-888-405-9721</a> – they will help you find a great last-minute timeshare rental deal.
				</p>
			</div>
		</div>
	</div>
</div>