<div class="contentHeader">About Us</div>
<div class="content">
	<div class="m">
		<p>
			Do you have a timeshare for rent? Are you looking for an affordable vacation somewhere spectacular? Resortmanagementwordwide.com has been the solution to timeshare renters. When you rent a timeshare you can enjoy luxury resorts and breathtaking destinations at a reasonable price.
		</p>
		<p>
			Without the reoccurring fees and taxes, finding a timeshare for rent will change the way you vacation forever. Over the past decade Resortmanagementworldwide.com has worked to make it easy and affordable to travel. There are so many timeshares to choose from, why pay the reoccurring fees and taxes of ownership? With such a wide range of points, seasons and packages available, the experts at Resortmanagementworldwide.com are ready to help you find the timeshare rental that is right for you.
		</p>
	</div>
</div>
<div class="row">
	<div class="grid_6 col">
		<div class="contentHeader">Contact Us</div>
		<div class="content">
			<div class="m">
				<div>Phone Number: </div>
				<div><a href="tel:+1-888-405-9721">1-888-405-9721</a></div>
				<p></p>
				<div>Email: </div>
				<div><a href="mailto:customerservice@resortmanagementworldwide.com" target="_top">customerservice@resortmanagementworldwide.com</a></div>
			</div>
		</div>
	</div>
	<div class="grid_6 col">
		<div class="contentHeader">Inquiry</div>
		<div class="content">
			<form method="POST" action="<?=$_SERVER['REQUEST_URI'];?>">
				<div class="row m">
					<?=$this->inputText('firstName', array('label' => 'First Name'));?>
					<?=$this->inputText('lastName', array('label' => 'Last Name'));?>
				</div>
				<div class="row m">
					<?=$this->inputText('emailAddress', array('label' => 'Email Address'));?>
					<?=$this->inputText('cellPhone', array('label' => 'Phone Number'));?>
				</div>
				<div class="row m">
					<?=$this->textarea('message', array('label' => 'Message', 'class' => 'grid_12 col'));?>
				</div>
				<div class="row m">
					<?=$this->button('submit', array('content'=>'SUBMIT'));?>
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</div>