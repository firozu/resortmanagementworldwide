	<div class="clear"></div>
	</div>
	<div id="footer" class="smoothBackground">
		<div class="container row">
			<ul id="footerLinks" class="grid_8 col">
				<li><a href="<?=BASE_PATH;?>">Home</a></li>
			  	<li><a href="<?=BASE_PATH . '/properties/view';?>">Properties</a></li>
			  	<li><a href="<?=BASE_PATH . '/contact';?>">Contact Us</a></li>
			  	<li><a href="<?=BASE_PATH . '/faq';?>">FAQs</a></li>
			</ul>
			<div id="socialMediaLinkContainer" class="grid_4 col right">
				<a href="" class="socialMediaLink">
					<img src="<?=BASE_PATH . '/img/facebookIcon.png';?>">
				</a>
				<a href="" class="socialMediaLink">
					<img src="<?=BASE_PATH . '/img/twitterIcon.png';?>">
				</a>
				<a href="" class="socialMediaLink">
					<img src="<?=BASE_PATH . '/img/instagramIcon.png';?>">
				</a>
			</div>
			<div class="row">
				<div id="copyright" class="grid_12 col">
					Copyright &copy;2013 Resort Management Worldwide. All Rights Reserved
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</body>
</html>
