<table border="1">
	<?php
	//Display headers
	echo "<tr>";
	foreach($fields as $field) {
		if ($field != 'id') {
			echo "<th>$field</th>";
		}

	}
	?>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	<?php
	//display data
	foreach ($properties as $property) {
		$propertyId = $property['Property']['id'];
		$editPath = 'admin/properties/view/' . $propertyId;
		$deletePath = 'admin/properties/delete/' . $propertyId;

		echo "<tr>";
		foreach ($fields as $field) {
			if ($field != 'id') {
				echo "<td>{$property['Property'][$field]}</td>";
			}
		}
	?>
		<td><?=$html->link('Edit', $editPath);?></td>
		<td><?=$html->link('Delete', $deletePath, TRUE);?></td>
	</tr>
	<?php }	?>
</table>
<?=$html->link('+ Add', 'admin/properties/add');?>