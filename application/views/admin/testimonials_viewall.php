<table border="1">
	<?php
	//Display headers
	echo "<tr>";
	foreach($fields as $field) {
		if ($field != 'id') {
			echo "<th>$field</th>";
		}

	}
	?>
		<th>Edit</th>
		<th>Delete</th>
	</tr>
	<?php
	//display data
	foreach ($testimonials as $testimonial) {
		$testimonialId = $testimonial['Testimonial']['id'];
		$editPath = 'admin/testimonials/view/' . $testimonialId;
		$deletePath = 'admin/testimonials/delete/' . $testimonialId;

		echo "<tr>";
		foreach ($fields as $field) {
			if ($field != 'id') {
				echo "<td>{$testimonial['Testimonial'][$field]}</td>";
			}
		}
	?>
		<td><?=$html->link('Edit', $editPath);?></td>
		<td><?=$html->link('Delete', $deletePath, TRUE);?></td>
	</tr>
	<?php }	?>
</table>
<?=$html->link('+ Add', 'admin/testimonials/add');?>