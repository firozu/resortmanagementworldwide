<div class="contentHeader">Add Property</div>
<div class="content">
	<form class="m" method="POST" action="<?=$_SERVER['REQUEST_URI'];?>"
		enctype="multipart/form-data">
		<div class='row'>
			<div class="grid_2 col bold">Unique Id: </div>
			<?=$this->inputText('uniqueId', $uniqueIdParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">Name: </div>
			<?=$this->inputText('name', $nameParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">City: </div>
			<?=$this->inputText('city', $cityParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">State: </div>
			<?=$this->inputText('state', $stateParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">Zip: </div>
			<?=$this->inputText('zip', $zipParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">Price: </div>
			<?=$this->inputText('price', $priceParams);?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">Bed: </div>
			<?=$this->inputText('bed', $bedParams);?>
		</div>
			<div class='row'>
			<div class="grid_2 col bold">Bath: </div>
			<?=$this->inputText('bath', $bathParams);?>
		</div>
			<div class='row'>
			<div class="grid_2 col bold">Sqft: </div>
			<?=$this->inputText('sqft', $sqftParams);?>
		</div>
		<div class='row'>
			<div class='grid_2 col bold'>Images: </div>
			<?php
				if (isset($id)) {
					$editPath = 'admin/properties/images/' . $id;
					echo $html->link('Edit Images', $editPath);
				} else {
					echo $this->inputText('images[]', $imageParams);
				}
			?>
		</div>
		<div class='row'>
			<div class="grid_2 col bold">Description: </div>
			<?=$this->textArea('description', $descriptionParams);?>
		</div>
		<div class='row'>
			<div class='grid_2 col'></div>
			<?=$this->button('save', array('content'=>'SAVE'));?>
		</div>
		<div class="clear"></div>
	</form>
</div>