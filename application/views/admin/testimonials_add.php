<div class="contentHeader">Add Testimonial</div>
<div class="content">
	<form class="m" method="POST"
		action="<?=$_SERVER['REQUEST_URI'];?>">
		<div class='row'>
			<div class="grid_1 col bold">Author</div>
			<?=$this->inputText('author', $authorParams);?>
		</div>
		<div class='row'>
			<div class="grid_1 col bold">Text</div>
			<?=$this->textArea('text', $textParams);?>
		</div>
		<div class='row'>
			<div class='grid_1 col'></div>
			<?=$this->button('save', array('content'=>'SAVE'));?>
		</div>

		<div class="clear"></div>
	</form>
</div>