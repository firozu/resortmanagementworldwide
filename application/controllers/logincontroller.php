<?php
/**
 * Controller for login functionality.
 */
class LoginController extends Controller {
	/**
	 * Post data from client.
	 * @var array
	 */
	private $_post = array();

	/**
	 * Index action handles post of user login and sets user id in session.
	 */
	public function index() {
		$this->_post = $_POST;

		if (isset($this->_post['login'])) {
			$this->handle();
		}
		$this->setParams($this->_post);
	}

	/**
	 * Handles post - checks if user email and password are valid.
	 */
	private function handle() {
		$url = isset($_SESSION[self::SESSION_ORIG_URL]) ?
				$_SESSION[self::SESSION_ORIG_URL] :	BASE_PATH;
		$userModel = new User();
		$userModel->where('email', $this->_post['email']);
		$userModel->where('password', md5($this->_post['password']));
		$userArr = $userModel->search();

		if (isset($userArr) && count($userArr) == 1) {
			$user = $userArr[0];
			unset($_SESSION[self::SESSION_ORIG_URL]);
			$_SESSION[self::SESSION_USER_ID] = $user['User']['id'];
			$this->redirect($url);
		}
	}

	/**
	 * Sets input fields on login form.
	 *
	 * @param array $post Post data to set on input fields.
	 */
	private function setParams($post) {
		$email = isset($post['email']) ? $post['email'] : NULL;
		$emailParams = array(
			'value' => $email,
			'label' => "Email",
			'type' => "email",
		);
		$passwordParams = array(
			'label' => 'Password',
			'type' => 'password',
		);
		$loginParams = array(
			'value' => 'login',
			'content' => "Log In",
		);

		$this->set('emailParams', $emailParams);
		$this->set('passwordParams', $passwordParams);
		$this->set('loginParams', $loginParams);
	}
}