<?php
/**
 * Controller for admin functionality.
 */
class AdminController extends UserController {
	/**
	 * Directory for property images.
	 * @var string
	 */
	public $_propertyImageDir;

	/**
	 * Post data from client.
	 * @var array
	 */
	private $_post = array();

	/**
	 * Check if the user is admin and display error message if not.
	 */
	public function beforeAction() {
		parent::beforeAction();
		if (!$this->isAdmin()) {
			$this->forward(
				'error',
				'message',
				array(
					'message'=>'You do not have permission to view this page.'
				)
			);
		}
		$this->_propertyImageDir = ROOT . DS . 'public' . DS . 'img' . DS
			. 'properties';
	}

	public function index() {

	}

	/**
	 * Handles adding a testimonial or updating if $testimonial is set.
	 *
	 * @param  array $testimonial
	 */
	public function testimonials_add($testimonial = NULL) {
		$this->_post = $_POST;

		if (isset($this->_post['save'])) {
			$this->saveTestimonial(
				$this->_post,
				$testimonial['Testimonial']['id']
			);
		}
		$this->setParams($testimonial);
	}

	/**
	 * Shows all testimonials or one testimonial for edit if $id is set.
	 *
	 * @param  int $id
	 */
	public function testimonials_view($id=NULL) {
		if (!isset($id)) {
			$this->forward('admin', 'testimonials_viewall');
		} else {
			$testimonialModel = new Testimonial();
			$testimonialModel->id = $id;
			$testimonial = $testimonialModel->search();

			if (count($testimonial) == 1) {
				$this->forward('admin', 'testimonials_add', array($testimonial));
			}
		}
	}

	/**
	 * Displays all testimonials
	 */
	public function testimonials_viewall() {
		$testiminialModel = new Testimonial();
		$testimonials = $testiminialModel->search();
		$fields = $testiminialModel->getFields();

		$this->set('testimonials', $testimonials);
		$this->set('fields', $fields);
	}

	/**
	 * Deletes the testimonial by the given id.
	 *
	 * @param  int $id
	 */
	public function testimonials_delete($id) {
		$testimonial = new Testimonial();
		$testimonial->id = $id;
		echo $id;

		if ($testimonial->delete()) {
			$this->redirect(BASE_PATH . '/admin/testimonials/view');
		}
	}

	/**
	 * Set testimonial parameters on the add/update form for a testimonial
	 *
	 * @param array $testimonial
	 */
	private function setParams($testimonial = NULL) {
		$authorParams = array(
			'value' => $testimonial['Testimonial']['author'] ?: NULL
		);
		$textParams = array(
			'type' => 'textarea',
			'content' => $testimonial['Testimonial']['text'] ?: NULL,
		);

		$this->set('authorParams', $authorParams);
		$this->set('textParams', $textParams);
	}

	private function saveTestimonial($post, $id = NULL) {
		$testimonial = new Testimonial();
		if (isset($id)) {
			$testimonial->id = $id;
		}
		$testimonial->author = $post['author'];
		$testimonial->text = $post['text'];

		if ($testimonial->save()) {
			$this->redirect(BASE_PATH . '/admin/testimonials/view');
		} else {
			echo "BOOO";
		}
	}

	/**
	 * Handles adding/updating a property.  If $property is set it forwards to
	 * the update action else it forwards to the add action.
	 *
	 * @param  array $property
	 */
	public function properties_add($property = NULL) {
		$this->_post = $_POST;

		if (isset($this->_post['save'])) {
			$this->saveProperty(
				$this->_post,
				$property['Property']['id']
			);
		}
		$this->setPropertyParams($property);
	}

	/**
	 * Handles the view action of properties. If id is set goes to updating a
	 * single property
	 *
	 * @param  int $id
	 */
	public function properties_view($id=NULL) {
		if (!isset($id)) {
			$this->forward('admin', 'properties_viewall');
		} else {
			$propertyModel = new Property();
			$propertyModel->id = $id;
			$property = $propertyModel->search();

			if (count($property) == 1) {
				$this->forward('admin', 'properties_add', array($property));
			}
		}
	}

	/**
	 * Displays all properties in grid form.
	 */
	public function properties_viewall() {
		$propertyModel = new Property();
		$properties = $propertyModel->search();
		$fields = $propertyModel->getFields();

		$this->set('properties', $properties);
		$this->set('fields', $fields);
	}

	/**
	 * Deletes a property by the given id.
	 * @param  int $id
	 */
	public function properties_delete($id) {
		$property = new Property();
		$property->id = $id;
		echo $id;

		if ($property->delete()) {
			$this->redirect(BASE_PATH . '/admin/properties/view');
		}
	}

	/**
	 * Handles the properties_images request.
	 *
	 * @param  string $id
	 */
	public function properties_images($id) {
		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->handleImageRequest($id);
		}
		$this->setImageParams($id);
	}

	/**
	 * Sets the params on the properties add/update form.
	 *
	 * @param array $property
	 */
	private function setPropertyParams($property = NULL) {
		$nameParams = array(
			'value' => $property['Property']['name']
		);
		$cityParams = array(
			'value' => $property['Property']['city']
		);
		$stateParams = array(
			'value' => $property['Property']['state']
		);
		$zipParams = array(
			'value' => $property['Property']['zip'],
			'type' => 'number',
		);
		$priceParams = array(
			'value' => $property['Property']['price'],
			'type' => 'number',
		);
		$bedParams = array(
			'value' => $property['Property']['bed'],
			'type' => 'number',
		);
		$bathParams = array(
			'value' => $property['Property']['bath'],
			'type' => 'number',
		);
		$sqftParams = array(
			'value' => $property['Property']['sqft'],
			'type' => 'number',
		);
		$descriptionParams = array(
			'type' => 'textarea',
			'rows' => 10,
			'content' => $property['Property']['description'],
		);
		$imageParams = array (
			'type' => 'file',
			'multiple' => TRUE,
		);
		$uniqueIdParams = array(
			'value' => $property['Property']['unique_id'],
			'type' => 'number',
		);

		$this->set('nameParams', $nameParams);
		$this->set('cityParams', $cityParams);
		$this->set('stateParams', $stateParams);
		$this->set('zipParams', $zipParams);
		$this->set('priceParams', $priceParams);
		$this->set('bedParams', $bedParams);
		$this->set('bathParams', $bathParams);
		$this->set('sqftParams', $sqftParams);
		$this->set('imageParams', $imageParams);
		$this->set('descriptionParams', $descriptionParams);
		$this->set('uniqueIdParams', $uniqueIdParams);
		$this->set('id', $property['Property']['id']);
	}

	/**
	 * Adds/Updates a property
	 *
	 * @param  array $post
	 * @param  int $id
	 */
	private function saveProperty($post, $id = NULL) {
		$property = new Property();

		$property->id = $id;
		$property->unique_id = $post['uniqueId'];
		$property->name = $post['name'];
		$property->city = $post['city'];
		$property->state = $post['state'];
		$property->zip = $post['zip'];
		$property->description = $post['description'];
		$property->bed = $post['bed'];
		$property->bath = $post['bath'];
		$property->sqft = $post['sqft'];
		$property->price = $post['price'];

		if ($property->save()) {
			if (!isset($id)) {
				$propertyId = $property->getLastId();
			}
			$dir = $this->createPropertyDirectory($propertyId);
			$this->saveFiles($dir);
			$this->redirect(BASE_PATH . '/admin/properties/view');
		} else {
			throw new Exception("Could not create Property");
		}
	}

	/**
	 * Creates the image directory for the given property id. The folder will be
	 * name property_{$propertyId}
	 *
	 * @param  String $propertyId
	 * @return String
	 */
	private function createPropertyDirectory($propertyId) {
		$dir = $this->_propertyImageDir . DS . 'property_' . $propertyId;

		if (!is_dir($dir)) {
			mkdir($dir, 0755);
		}

		if (!is_dir($dir)) {
			throw new Exception('Could not create directory' . $dir);
		}
		return $dir;
	}

	/**
	 * Saves Image files to the given directory.
	 *
	 * @param  string $dir
	 */
	private function saveFiles($dir) {
		if (isset($dir) && isset($_FILES['images'])) {
			$types = array("image/jpeg","image/jpg","image/png", "image/gif");

			foreach ($_FILES['images']['tmp_name'] as $key => $tempName) {
				$fileName = $_FILES['images']['name'][$key];
				$fileType = $_FILES['images']['type'][$key];
				if (in_array($fileType, $types)) {
					move_uploaded_file($tempName, $dir . DS . $fileName);
				}
			}
		}
	}

	/**
	 * Sets parameters for images on given property
	 *
	 * @param string $id
	 */
	private function setImageParams($id) {
		$dir = $this->_propertyImageDir . DS . 'property_' . $id;
		$path = BASE_PATH . '/public/img/properties/property_' . $id;
		$images = array_diff(scandir($dir), array('..', '.'));
		$imageParams = array (
			'type' => 'file',
			'multiple' => TRUE,
		);

		$this->set('images', $images);
		$this->set('path', $path);
		$this->set('imageParams', $imageParams);
	}

	/**
	 * Handles submit on Image update
	 *
	 * @param  string $id
	 */
	private function handleImageRequest($id) {
		if (isset($_POST['delete'])) {
			$images = $_POST['images'];
			foreach ($images as $image) {
				$this->deleteImage($id, $image);
			}
		} else if (isset($_POST['add'])) {
			$dir = $this->_propertyImageDir . DS . 'property_' . $id;
			$this->saveFiles($dir);
		}
	}

	/**
	 * Deletes image with given directory and name.
	 *
	 * @param  string $id
	 * @param  string $image
	 */
	private function deleteImage($id, $image) {
		$path = $this->_propertyImageDir . DS . 'property_' . $id;
		$fileName = $path . DS . $image;
		unlink($fileName);
	}

	private function generateUniqueId($digits) {
		$min = pow(10, $digits - 1);
    	$max = pow(10, $digits) - 1;
    	return mt_rand($min, $max);
	}
}