<?php
/**
 * Default controller for the index of the website.
 */
class IndexController extends Controller {

	/**
	 * Index action.
	 */
	public function index() {
		$this->setTitle("Resort Management Worldwide");
		$this->setParams();
	}

	private function setParams() {
		$dir = ROOT . DS . 'public' . DS . 'img' . DS . 'slides';
		$images = array_diff(scandir($dir), array('..', '.'));
		$path = BASE_PATH . '/img/slides';

		$testimonialModel = new Testimonial();
		$testimonials = $testimonialModel->search();

		$properties = $this->getLatestProperties();

		$this->set('dir', $path);
		$this->set('images', $images);
		$this->set('testimonials', $testimonials);
		$this->set('properties', $properties);
	}

	private function getLatestProperties() {
		$propertyModel = new Property();
		$propertyModel->orderBy('id', 'DESC');
		$propertyModel->setLimit(6);
		$propertyModel->setPage(1);

		return $propertyModel->search();
	}

}