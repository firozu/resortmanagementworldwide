<?php
/**
 * Controller for viewing properties
 */
class PropertiesController extends Controller {


	public function view($uniqueId=NULL) {
		if (!isset($uniqueId)) {
			$this->forward('properties', 'viewall');
		} else {
			$this->addToCss('properties-details');
			$propertyModel = new Property();
			$propertyModel->where('unique_id', $uniqueId);
			$property = $propertyModel->search()[0];
			$id = $property['Property']['id'];
			$dir = BASE_PATH . "/img/properties/property_" . $id;
			$images = Property::getImages($id);

			$this->set('property', $property);
			$this->set('dir', $dir);
			$this->set('images', $images);
		}
	}

	public function viewAll() {
		$propertyModel = new Property();
		$propertyModel->orderBy('id', "DESC");
		$properties = $propertyModel->search();

		$this->set('properties', $properties);
	}

	public function search($uniqueId) {
		$this->setTitle("Search | Resort Management Worldwide");
		$propertyModel = new Property();
		$propertyModel->where('unique_id', $uniqueId);
		$property = $propertyModel->search();

		if (count($property) == 1) {
			$this->forward('properties', 'view', array('uniqueId' => $uniqueId));
		} else {
			$this->forward(
				'error',
				'message',
				array(
					'message'=>'Sorry! We cannot find a property with that specified ID.'
				)
			);
		}
	}



}