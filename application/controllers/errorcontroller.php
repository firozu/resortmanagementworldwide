<?php
/**
 * Controller for handling errors.
 */
class ErrorController extends Controller {

	public function index() {

	}

	public function message($message) {
		$this->set('message', $message);
	}
}