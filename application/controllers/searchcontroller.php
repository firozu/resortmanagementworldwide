<?php
/**
 * Controller that handles searching.
 */
class SearchController extends Controller {

	/**
	 * Post data from client.
	 * @var array
	 */
	private $_post = array();

	/**
	 * Index action.
	 */
	public function index($uniqueId) {
		$this->setTitle("Search | Resort Management Worldwide");
		$propertyModel = new Property();
		$propertyModel->where('unique_id', $uniqueId);
		$property = $propertyModel->search();

		if (count($property) == 1) {
			$this->forward('properies', 'view', array('uniqueId' => $uniqueId));
		} else {
			$this->forward(
				'error',
				'message',
				array(
					'message'=>'Sorry! We cannot find a property with that specified ID.'
				)
			);
		}
	}

}