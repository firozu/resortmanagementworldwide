<?php
/**
 * ViewHelper that displays testimonial list.
 */
class TestimonialsList extends ViewHelper {
	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($testimonials) {
		$this->_view->addToCss('testimonials-list');
		$this->_view->addToJs('testimonials-list');
		$quote = BASE_PATH . '/img/quote.png';
		$html = <<<HTML
		<ul class="testimonialsList m">
HTML;
		foreach ($testimonials as $testimonial) {
			$html .= <<<HTML
			<li class="row">
				<div class="quoteContainer">
					<img class="testimonialQuote" src="$quote">
				</div>
				<div class="textContainer">
					<div class="testimonialText">
						{$testimonial['Testimonial']['text']}
					</div>
					<div class="right">
						- {$testimonial['Testimonial']['author']}
					</div>
				</div>
				<div class="clear"></div>
			</li>
HTML;
		}

		return $html;
	}
}