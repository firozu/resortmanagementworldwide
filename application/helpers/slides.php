<?php
/**
 * ViewHelper that creates a slideshow.
 */
class Slides extends ViewHelper {
	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($id, $dir, $images) {
		$this->_view->addToJs('jquery/jquery.slides.min');
		$this->_view->addToJs('slides');

		$html = <<<HTML
		<div id="$id" class="slides">
HTML;
		foreach ($images as $image) {
			$file = $dir . DS . $image;
			$html .= <<<HTML
			<img src="$file">
HTML;
		}
		$html .= <<<HTML
		</div>
HTML;

		return $html;
	}
}