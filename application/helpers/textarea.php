<?php
/**
 * ViewHelper that creates the textarea element with proper styling
 */
class TextArea extends ViewHelper {
	/**
	 * Default parameters for input text view helper.
	 * @var array
	 */
	private $_defaults = array(
		'class' => 'grid_6 col',
		'label' => NULL,
		'placeholder' => NULL,
		'rows' => '5',
		'cols' => NULL,
		'readonly' => FALSE,
		'content' => NULL,
	);

	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($name, $params) {
		$options = array_merge($this->_defaults, $params);
		$cols = isset($options['cols']) ? "cols='{$options['cols']}'" : NULL;
		$html = "<div class='{$options['class']}' >";

		if ($options['label']) {
			$html .= "<label class='grid_12 left' for='{$name}'>"
				. "{$options['label']}</label>";
		}
		$html .= "<textarea name='$name' id='$name' rows='{$options['rows']}'"
			." {$cols} placeholder='{$options['placeholder']}' class='grid_12'"
			." data-theme='a'>"
			. "{$options['content']}</textarea></div>";

		return $html;
	}
}