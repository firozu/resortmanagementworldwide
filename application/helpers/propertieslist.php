<?php
/**
 * ViewHelper that displays properties list.
 */
class PropertiesList extends ViewHelper {

	public function render($properties) {
		$this->_view->addToCss('properties-list');
		$count = 0;
		$html = <<<HTML
		<div class="propertiesList m">
			<ul class="row">
HTML;
		foreach ($properties as $property) {
			$image = Property::getDefaultImage($property['Property']['id']);
			$price = '$' . number_format($property['Property']['price']);
			$sqft = number_format($property['Property']['sqft']);
			$desc = $property['Property']['description'];
			$description = strlen($desc) > 150 ? substr($desc,0, 150) . '...' : $desc;
			$link = BASE_PATH . '/properties/view/' . $property['Property']['unique_id'];
			$count++;

			$html .= <<<HTML
			<li>
				<div class="name row">
					{$property['Property']['name']}
				</div>
				<div class="row">
					<div class="grid_4 col">
						<img src="$image" >
					</div>
					<div class="grid_5 col">
						<div class="m text-large">
							{$property['Property']['city']}, {$property['Property']['state']}
						</div>
						<div class="m">
							{$property['Property']['bed']} Bed / {$property['Property']['bath']} Bath / {$sqft}sqft
						</div>
						<div class="m italic">
							{$description}
						</div>
					</div>
					<div class="grid_3 col">
						<div class="m text-large">
							{$price}
						</div>
						<div class="uniqueId m">
							ID: {$property['Property']['unique_id']}
						</div>
						<div class="center m">
							<a class="grid_12 btnGray" href="$link">View Details</a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</li>
HTML;
		}

		$html .= <<<HTML
		</ul></div>
HTML;

		return $html;
	}
}