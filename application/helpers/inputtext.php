<?php
/**
 * ViewHelper that creates input text/password element with proper styling.
 */
class InputText extends ViewHelper {
	/**
	 * Default parameters for input text view helper.
	 * @var array
	 */
	private $_defaults = array(
		'class' => 'grid_6 col',
		'type' => 'text',
		'label' => NULL,
		'placeholder' => NULL,
		'value' => NULL,
		'multiple' => NULL,
	);

	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($name, $params) {
		$options = array_merge($this->_defaults, $params);
		$multiple = isset($options['multiple']) ? "multiple=multiple" : NULL;
		$html = <<<HTML
			<div class="{$options['class']}" >
HTML;

		if ($options['label']) {
			$html .= <<<HTML
			<label class="grid_12 left" for="{$name}">
				 {$options['label']}</label>
HTML;
		}
		$html .= <<<HTML
		<input type="{$options['type']}" name="$name" id="$name"
			placeholder="{$options['placeholder']}" class="grid_12"
			value="{$options['value']}" $multiple /></div>
HTML;

		return $html;
	}
}