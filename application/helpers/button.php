<?php
/**
 * ViewHelper that creates a button element with proper styling.
 */
class Button extends ViewHelper {
	/**
	 * Default parameters for input text view helper.
	 * @var array
	 */
	private $_defaults = array(
		'class' => 'grid_3 col btnGray',
		'type' => 'submit',
		'value' => NULL,
		'content' => NULL,
	);

	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($name, $params) {
		$options = array_merge($this->_defaults, $params);
		$html = "<div class='{$options['class']}'>"
				. "<button type='{$options['type']}' name='$name' id='$name'"
					. "class='grid_12' value='{$options['value']}' data-theme='a'>"
					. "{$options['content']}"
				. "</button>"
			. "</div>";

		return $html;
	}
}