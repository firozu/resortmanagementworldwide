<?php
/**
 * ViewHelper that displays testimonial list.
 */
class PropertiesGrid extends ViewHelper {
	/**
	 * Renders the input text view helper.
	 *
	 * @param  String $name   Name and id of input field
	 * @param  array $params  Overriding options for this view helper
	 * @return String         HTML
	 */
	public function render($properties) {
		$this->_view->addToCss('properties-grid');
		$count = 0;
		$html = <<<HTML
		<div class="propertiesGrid m">
			<div class="row">
HTML;
		foreach ($properties as $property) {
			$image = Property::getDefaultImage($property['Property']['id']);
			$price = '$' . number_format($property['Property']['price']);
			$link = BASE_PATH . '/properties/view/' . $property['Property']['unique_id'];
			$count++;

			if ($count % 4 == 0) {
				$html .= <<<HTML
				</div>
				<div class="row">
HTML;
			}
			$html .= <<<HTML
			<div class="grid_4 col">
				<div class="row">
					<img src="$image" >
				</div>
				<div class="leftContainer">
					<div class="bedBathContainer m">
						<div class="text-xsmall">
							Bed / Bath
						</div>
						<div class="bedBath">
							{$property['Property']['bed']} / {$property['Property']['bath']}
						</div>
					</div>
					<div class="price">
						{$price}
					</div>
				</div>
				<div class="rightContainer">
					<div class="city">
						{$property['Property']['city']}
					</div>
					<div class="state text-xsmall">
						{$property['Property']['state']}
					</div>
					<div class="uniqueId">
						ID: {$property['Property']['unique_id']}
					</div>
				</div>
				<div class="center">
					<a class="grid_12 btnOrange" href="$link">View Details</a>
				</div>
			</div>
HTML;
		}

		$html .= <<<HTML
		</div><div class="clear"></div></div>
HTML;

		return $html;
	}
}