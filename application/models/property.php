<?php
class Property extends Model {

	public static function getDefaultImage($id) {
		$dir = ROOT . DS . 'public' . DS . 'img' . DS. 'properties' . DS
			. 'property_' . $id;
		$path = BASE_PATH . '/img/properties/property_' . $id;
		$files = scandir($dir, 1);

		return $path . '/' . $files[0];
	}

	public static function getImages($id) {
		$images = array();
		$dir = ROOT . DS . 'public' . DS . 'img' . DS. 'properties' . DS
			. 'property_' . $id;
		$path = BASE_PATH . '/img/properties/property_' . $id;
		$files = array_diff(scandir($dir), array('..', '.'));

		return $files;
	}
}