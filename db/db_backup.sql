CREATE TABLE `users` (
 `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
 `first_name` varchar(20) NOT NULL,
 `last_name` varchar(40) NOT NULL,
 `email` varchar(60) NOT NULL,
 `password` char(40) NOT NULL,
 `is_admin` tinyint(1) NOT NULL DEFAULT '0',
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1

CREATE TABLE 'testimonials' (
	'id' MEDIUMINT(8) unsigned NOT NULL AUTO_INCREMENT,
	'author' varchar(40) NOT NULL,
	'text' TEXT,
	PRIMARY KEY ('id')
);

CREATE TABLE properties (
	id MEDIUMINT(8) unsigned NOT NULL AUTO_INCREMENT,
	unique_id varchar(16) NOT NULL,
	name varchar(40) NOT NULL,
	address varchar(40),
	city varchar(20),
	state varchar(20),
	zip int(11),
	description TEXT,
	PRIMARY KEY (id),
	UNIQUE(unique_id)
);


ALTER TABLE properties ADD COLUMN bed TINYINT(1) DEFAULT 0;
ALTER TABLE properties ADD COLUMN bath TINYINT(1) DEFAULT 0;
ALTER TABLE properties ADD COLUMN price INT DEFAULT 0;
ALTER TABLE properties ADD COLUMN sqft MEDIUMINT(8) DEFAULT 0;
ALTER TABLE properties DROP COLUMN address;